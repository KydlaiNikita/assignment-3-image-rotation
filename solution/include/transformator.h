//
// Created by Мы on 04.02.2023.
//

#ifndef IMAGE_TRANSFORMER_TRANSFORMATOR_H
#define IMAGE_TRANSFORMER_TRANSFORMATOR_H
#include "bmp_manager.h"

struct image transform_image(struct image source_image);

#endif //IMAGE_TRANSFORMER_TRANSFORMATOR_H
