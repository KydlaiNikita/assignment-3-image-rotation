//
// Created by Мы on 03.02.2023.
//

#ifndef IMAGE_TRANSFORMER_FILE_MANAGER_H
#define IMAGE_TRANSFORMER_FILE_MANAGER_H

#include "stdio.h"
#include "stdlib.h"

// Перечисление статусов попытки открытия файла
enum OPEN_STATUS {
    OPEN_OK,
    OPEN_ERROR
};

// Перечисление статусов попытки закрытия файла
enum CLOSE_STATUS {
    CLOSE_OK,
    CLOSE_ERROR
};

// Пытается открыть файл
enum OPEN_STATUS open_file(FILE **file, const char *filename, const char *mode);

// Пытается закрыть файл
enum CLOSE_STATUS close_file(FILE *file);

#endif //IMAGE_TRANSFORMER_FILE_MANAGER_H
