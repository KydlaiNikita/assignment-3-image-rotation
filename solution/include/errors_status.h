//
// Created by Мы on 16.02.2023.
//

#ifndef IMAGE_TRANSFORMER_ERRORS_STATUS_H
#define IMAGE_TRANSFORMER_ERRORS_STATUS_H
#include "bmp_manager.h"
#include "file_manager.h"

static const char* write_image_errors[] = {
        [WRITE_DATA_ERROR] = "Unable to write file: invalid data",
        [WRITE_PADDING_ERROR] = "Unable to write file: invalid padding",
        [WRITE_EMPTY_DATA_ERROR] = "Unable to write file: data is empty"
};

static const char* read_source_image_errors[] = {
        [READ_INVALID_HEADER] = "Invalid source image header",
        [READ_INVALID_BITS] = "Invalid bits in source image",
        [READ_INVALID_SIGNATURE] = "Invalid source image signature"
};

static const char* open_image_errors[] = {
        [OPEN_ERROR] = "Unable to open source image for read"
};

static const char* close_image_errors[] = {
        [CLOSE_ERROR] = "Unable to close source image"
};

static const char* transform_image_error = "Transformation error: memory exception";

#endif //IMAGE_TRANSFORMER_ERRORS_STATUS_H
