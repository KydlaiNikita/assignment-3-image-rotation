//
// Created by Kydlai inc. on 02.02.2023.
//

#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H

#include "image.h"
#include "stdio.h"
#include <stdint.h>

// Структура, описывающая заголовок BMP-файла
#pragma pack(push,1)
struct bmp_header
{
    uint16_t bfType; //BM если это .bmp
    uint32_t bfileSize; //Размер файла
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize; //Размер BITMAPINFOHEADER в байтах
    uint32_t biWidth; //Размеры изображения
    uint32_t biHeight; //Размеры изображения
    uint16_t biPlanes;
    uint16_t biBitCount; //Размеры изображения
    uint32_t biCompression; //BI_RGB если не сжат
    uint32_t biSizeImage; //Размер графических данных в пикселях (может быть 0)
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed; //Количество цветов в палитре (может быть 0)
    uint32_t biClrImportant;
};
#pragma pack(pop)

// Перечисление статусов попытки чтения файла
enum READ_STATUS {
    READ_OK,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

// Перечисление статусов попытки записи в файл
enum WRITE_STATUS {
    WRITE_OK,
    WRITE_DATA_ERROR,
    WRITE_EMPTY_DATA_ERROR,
    WRITE_PADDING_ERROR
};

// Пытается прочитать BMP-файл
enum READ_STATUS from_bmp(FILE *input, struct image *image);

// Пытается записать в BMP-файл
enum WRITE_STATUS to_bmp(FILE *output, const struct image *image);

enum READ_STATUS read_bmp_header(FILE* file, struct bmp_header* bmp_header);

uint32_t get_padding(const uint32_t width);

struct bmp_header generate_header(const struct image* image);

#endif //IMAGE_TRANSFORMER_BMP_H
