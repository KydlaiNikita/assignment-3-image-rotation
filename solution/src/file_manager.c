//
// Created by Kydlai inc. on 03.02.2023.
//

#include "file_manager.h"

// Пытается открыть файл и возвращает статус
enum OPEN_STATUS open_file(FILE **file, const char *filename, const char *mode) {
    *file = fopen(filename, mode);
    if (!*file) {
        fprintf(stderr, "%s\n", filename);
        return OPEN_ERROR;
    } else {
        return OPEN_OK;
    }
}

// Пытается закрыть файл и возвращает статус
enum CLOSE_STATUS close_file(FILE *file) {
    if (fclose(file) == EOF) {
        return CLOSE_ERROR;
    } else {
        return CLOSE_OK;
    }
}
