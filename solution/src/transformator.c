//
// Created by Kydlai inc. on 04.02.2023.
//
#include "transformator.h"
struct image transform_image(struct image source_image){
    struct pixel* new_data = NULL;
    new_data = malloc(sizeof(struct pixel) * source_image.width * source_image.height);
    if(new_data == NULL){
     delete_image(&source_image);
     return source_image;
    }
    for(size_t i = 0; i < source_image.height; ++i){
        for (size_t j = 0; j < source_image.width; ++j) {
            new_data[source_image.height * j + (source_image.height - 1 - i)] = source_image.data[i * source_image.width + j];
        }
    }
    delete_image(&source_image);
    //fprintf(stderr, "%s", "Image rotated\n");
    return set_image(source_image.height, source_image.width, new_data);
}
