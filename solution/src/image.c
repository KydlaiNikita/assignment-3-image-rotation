//
// Created by Kydlai inc. on 03.02.2023.
//
#include "image.h"

struct image set_image(uint32_t width, uint32_t height, struct pixel* data){
    if(data == NULL)
        return (struct image) {.width = 0, .height = 0, .data = NULL};
    return (struct image) {.width = width, .height = height, .data = data};
}

struct image create_image(uint32_t width, uint32_t height){
    struct pixel* data = malloc(sizeof(struct pixel) * width * height);
    return set_image(width, height, data);
}

void delete_image(struct image* image){
    free(image->data);
    image->data = NULL;
}
