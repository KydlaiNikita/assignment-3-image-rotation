#include "transformator.h"
#include "errors_status.h"

int main( int argc, char** argv ) {

    if(argc != 3) {
        fprintf(stderr, "Required 2 arguments: <source-image-path> <transformed-image-path>\n");
        return 1;
    }

    FILE *input_file, *output_file;

    enum OPEN_STATUS open_source_image_status = open_file(&input_file, argv[1], "rb");

    if(open_source_image_status != OPEN_OK) {
        fprintf(stderr, "%s", open_image_errors[open_source_image_status]);
        return 2;
    }

    struct image image;

    enum READ_STATUS read_source_image_status = from_bmp(input_file, &image);
    if(read_source_image_status != READ_OK){
        fprintf(stderr, "%s", read_source_image_errors[read_source_image_status]);
        return 3;
    }

    enum CLOSE_STATUS close_source_image_status = close_file(input_file);
    if(close_source_image_status != CLOSE_OK) {
        fprintf(stderr, "%s", close_image_errors[close_source_image_status]);
    }

    enum OPEN_STATUS open_result_image_status = open_file(&output_file, argv[2], "wb");
    if(open_result_image_status != OPEN_OK) {
        fprintf(stderr, "%s", open_image_errors[open_result_image_status]);
        return 4;
    }


    image = transform_image(image);
    if(image.data == NULL){
        fprintf(stderr, "%s", transform_image_error);
        return 5;
    }

    enum WRITE_STATUS write_result_image_status = to_bmp(output_file, &image);
    if(write_result_image_status != WRITE_OK){
        fprintf(stderr, "%s", write_image_errors[write_result_image_status]);
        delete_image(&image);
        return 6;
    }

    delete_image(&image);

    enum CLOSE_STATUS close_result_image_status = close_file(output_file);
    if(close_result_image_status != CLOSE_OK) {
        fprintf(stderr, "%s", close_image_errors[close_result_image_status]);
    }

    return 0;
}
