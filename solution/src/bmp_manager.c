//
// Created by Kydlai inc. on 02.02.2023.
//
#include "bmp_manager.h"
#define BM_CODE 19778 //bfType в файле .bmp должен быть равен BM, то есть 19778 в численном представлении
#define PADDING_SIZE 4
#define IMAGE_PLANES 1
#define IMAGE_BIT_COUNT 24
#define HEADER_OFF_BITS 54
#define HEADER_SIZE 40

enum READ_STATUS read_bmp_header(FILE* file, struct bmp_header* bmp_header){
    if(fread(bmp_header, sizeof(struct bmp_header), 1, file) == 1
            && bmp_header->bfType == BM_CODE && bmp_header->biBitCount == 24)
        return READ_OK;
    else
        return READ_INVALID_HEADER;
}

uint32_t get_padding(const uint32_t width){
    return (PADDING_SIZE - (width * sizeof (struct pixel) % PADDING_SIZE)) % PADDING_SIZE;
}

size_t get_image_size(const struct image* image){
    return (image->height*((get_padding(image->width) + image->width * sizeof(struct pixel))));
}

size_t get_file_size(const struct image* image){
    return get_image_size(image) + sizeof(struct bmp_header);
}

struct bmp_header generate_header(const struct image* image){
    return (struct bmp_header){
        .bfType = BM_CODE,
        .bfileSize = get_file_size(image),
        .bfReserved = 0,
        .bOffBits = HEADER_OFF_BITS,
        .biSize = HEADER_SIZE,
        .biWidth = image->width,
        .biHeight = image->height,
        .biPlanes = IMAGE_PLANES,
        .biBitCount = IMAGE_BIT_COUNT,
        .biCompression = 0,
        .biSizeImage = get_image_size(image),
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0
    };
}

enum READ_STATUS from_bmp(FILE *input, struct image *image){
    struct bmp_header bmp_header = {0};
    if(read_bmp_header(input, &bmp_header) != READ_OK)
        return READ_INVALID_HEADER;

    *image = create_image(bmp_header.biWidth, bmp_header.biHeight);

     if(image->data == NULL){
        delete_image(image);
        return READ_INVALID_BITS;
    }

    const uint8_t padding = get_padding(bmp_header.biWidth);

    for(size_t i = 0; i < bmp_header.biHeight; i++) {
        for (size_t j = 0; j < bmp_header.biWidth; j++) {
            if (fread(&(image->data[image->width * i + j]), sizeof(struct pixel), 1, input) != 1){
                delete_image(image);
                return READ_INVALID_BITS;
            }
        }
        if (fseek(input, padding, SEEK_CUR) != 0){
            delete_image(image);
            return READ_INVALID_SIGNATURE;
        }
    }
    return READ_OK;
}

enum WRITE_STATUS to_bmp(FILE *output, const struct image *image){
    struct bmp_header bmp_header = generate_header(image);
    if(!fwrite(&bmp_header, sizeof(struct bmp_header), 1, output)
       || (fseek(output, bmp_header.bOffBits, SEEK_SET)))
        return WRITE_DATA_ERROR;


    const uint8_t padding = get_padding(image->width);

    if(image->data!=NULL){
        for(size_t i = 0; i < image->height; i++){
            if (fwrite(image->data + (i * image->width), image->width * sizeof(struct pixel), 1, output) < 1)
                return WRITE_DATA_ERROR;
            //fprintf(stderr, "%d\n", padding);
            uint32_t tmp = 0;
            if (fwrite(&tmp, 1, padding, output) < padding)
                return WRITE_PADDING_ERROR;

        }
    } else
        return WRITE_EMPTY_DATA_ERROR;
    return WRITE_OK;
}
